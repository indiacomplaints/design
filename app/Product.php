<?php

namespace App;
use App\Category;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
        'name', 'cat_id','description',"price","image"
    ];

    public function posts()
    {
        return $this->hasManyThrough('App\Category', 'App\Product','cat_id','id');
    }
}
